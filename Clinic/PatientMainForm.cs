﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinic
{
    public partial class PatientMainForm : Form
    {
        LoginForm loginForm = new LoginForm();

        //LoginForm loginForm = Owner as LoginForm;
        //LoginForm.Pat
        //if(loginForm != null)
        //{
        //    string s = main.textBox1.Text;
        //        main.textBox1.Text = "OK";
        //}


    public PatientMainForm()//(Patient patient) //todo
        {
            InitializeComponent();
            //this.patient = patient;

        }

        private void PatientMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void makeAppointmentButton_Click(object sender, EventArgs e)
        {
            //this.Hide();
            PatientAppointmentForm patientAppointmentForm = new PatientAppointmentForm();
            patientAppointmentForm.Show();
        }

        private void appointmentHistButton_Click(object sender, EventArgs e)
        {
            //this.Hide();
            AppointmentHistoryForm appointmentHistoryForm = new AppointmentHistoryForm();
            appointmentHistoryForm.Show();
        }

        private void PatientMainForm_Load(object sender, EventArgs e)
        {
            this.fioLable.Text = /*Patient.user_id +*/ Patient.last_name + " " + Patient.name +  " " + Patient.patronymic;
        }
    }
}
