﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinic
{
    public partial class AppointmentHistoryForm : Form
    {
        DB db = new DB();
        //todo
        String format = "{0, -20}{1, -20}{2, -35}{3, -100}";

        public AppointmentHistoryForm()
        {
            InitializeComponent();
        }

        private void AppointmentHistoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide(); 
            //PatientMainForm patientMainForm = new PatientMainForm();
            //patientMainForm.Show();
        }

        private void AppointmentHistoryForm_Load(object sender, EventArgs e)
        {
            //appointmentsListBox.DataSource = ;//

            try
            {
                using (SqlConnection connection = db.getConnection())
                {
                    connection.Open();
                    string query = "SELECT date, " +
                                    "s.start_time, " +
                                    "sp.specialty, " +
                                    "(d.last_name + ' ' + d.name + ' ' + d.patronymic) AS doctor " +
                                    "FROM schedule s JOIN appointments a " +
                                    "ON a.schedule_id = s.id " +
                                    "JOIN doctors d " +
                                    "JOIN specialties sp " +
                                    "ON d.specialty_id = sp.id " +
                                    "ON d.id = s.doctor_id " +
                                    "WHERE a.patient_id = @pat_id ORDER BY date; "; //todo
                    SqlCommand command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@pat_id", Patient.id);

                    SqlDataReader reader = command.ExecuteReader();

                    appointmentsListBox.Items.Add(String.Format(format, "Дата", "Время", "Вр. профиль", "ФИО врача"));//todo text, text alignment

                    while (reader.Read())
                    {
                        // todo
                        string date = Convert.ToDateTime(reader["date"]).ToShortDateString();
                        string time = reader["start_time"].ToString();
                        string spec = reader["specialty"].ToString(); ;
                        string doc = reader["doctor"].ToString(); ;
                        
                        string newItem = String.Format(format, date, time, spec, doc);
                        appointmentsListBox.Items.Add(newItem); //todo alignment
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show((ex.Message));
            }
        }
    }
}
