﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinic
{
    public partial class AdminMainForm : Form
    {
        public AdminMainForm()
        {
            InitializeComponent();
        }

        private void AdminMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void appointmentsButton_Click(object sender, EventArgs e)
        {
            AppointmentsTableForm appointmentsTableForm = new AppointmentsTableForm();
            appointmentsTableForm.Show();
        }


        private void usersButton_Click_1(object sender, EventArgs e)
        {
            UsersTableForm usersTableForm = new UsersTableForm();
            usersTableForm.Show();
        }

        private void specialtiesButton_Click(object sender, EventArgs e)
        {
            SpecialtiesTableForm specialtiesTableForm = new SpecialtiesTableForm();
            specialtiesTableForm.Show();
        }

        private void scheduleButton_Click(object sender, EventArgs e)
        {
            ScheduleTableForm scheduleTableForm = new ScheduleTableForm();
            scheduleTableForm.Show();
        }

        private void rolesButton_Click(object sender, EventArgs e)
        {
            Роли rolesTableForm = new Роли();
            rolesTableForm.Show();
        }

        private void patientButton_Click(object sender, EventArgs e)
        {
            PatientsTableForm patientsTableForm = new PatientsTableForm();
            patientsTableForm.Show();
        }

        private void doctorsButton_Click(object sender, EventArgs e)
        {
            DoctorsTableForm doctorsTableForm = new DoctorsTableForm();
            doctorsTableForm.Show();
        }
    }
}
