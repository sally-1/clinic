﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinic
{
    public partial class PatientAppointmentForm : Form
    {
        public PatientAppointmentForm()
        {
            InitializeComponent();
        }

        DB db = new DB();
        public List<spec> specs;// = new List<spec>();
        List<doctor> doctors = new List<doctor>();
        public List<schedule> schedules;// = new List<schedule>();

        public string selectedSpecId;
        public string selectedDocId;
        //TODO params for appointment insert
        public string selectedScheduleId; //todo
        public DateTime selectedDate; //qw data type
        //public DateTime today = DateTime.Today;


        public struct doctor //qw
        {
            public string id;
            public string fio;
        }
        public struct spec //qw
        {
            public string id;
            public string name;
        }
        public struct appointments //TODO
        {

        }

        private void PatientAppointmentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            //PatientMainForm patientMainForm = new PatientMainForm();
            //patientMainForm.Show();
        }

        private void appointButton_Click(object sender, EventArgs e)
        {

            if (docComboBox.SelectedItem == null || specComboBox.SelectedItem == null ||
                dateComboBox.SelectedItem == null || timeComboBox.SelectedItem == null)
            {
                MessageBox.Show("Заполните все поля!", "Пустые поля", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (MessageBox.Show("Подтвердить запись?", "Записаться", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                            == DialogResult.Yes) 
            {
                try
                {
                    using (SqlConnection connection = db.getConnection())
                    {
                        connection.Open();
                        string query = "INSERT INTO appointments(patient_id, date, schedule_id) VALUES(@pat_id, @date, @sch_id)";
                        SqlCommand insertAppointment = new SqlCommand(query, connection);
                        insertAppointment.Parameters.AddWithValue("@pat_id", Patient.id);
                        insertAppointment.Parameters.AddWithValue("@date", selectedDate.Date); //qw test data type sql/c#
                        insertAppointment.Parameters.AddWithValue("@sch_id", selectedScheduleId);

                        int RowsAffected = insertAppointment.ExecuteNonQuery();

                        if (RowsAffected == 1) //qw
                        {
                            MessageBox.Show("Запись выполнена успешно!", "Успешно", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            dateComboBox.Items.Clear();
                            timeComboBox.Items.Clear();
                            docComboBox.Items.Clear();
                        }
                        
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show((ex.Message));
                }
            }
        }

        public static DateTime GetNextWeekday(DateTime start, DayOfWeek day) // todo use
        {
            // https://stackoverflow.com/questions/6346119/datetime-get-next-tuesday
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }

        public static DayOfWeek ConvertIntsToDaysOfWeek(int day)
        {
            if (day == 1)
                return DayOfWeek.Monday;
            else if (day == 2)
                return DayOfWeek.Tuesday;
            else if (day == 3)
                return DayOfWeek.Wednesday;
            else if (day == 4)
                return DayOfWeek.Thursday;
            else if (day == 5)
                return DayOfWeek.Friday;
            else if (day == 6)
                return DayOfWeek.Saturday;
            else if (day == 7)
                return DayOfWeek.Sunday;
            else
                throw new Exception("Wrong day");
        }

        public List<doctor> GetDoctors(string spec_id)
        {
            using (SqlConnection connection = db.getConnection())
            {
                //doctors = new List<doctor>();//
                connection.Open();
                string query = "SELECT * FROM doctors WHERE specialty_id = @spec"; //todo
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@spec", spec_id); //qw

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    doctor doc = new doctor();
                    doc.fio = reader["last_name"].ToString() + " " + reader["name"].ToString() + " " + reader["patronymic"].ToString();
                    doc.id = reader["id"].ToString();
                    doctors.Add(doc);
                }

                return doctors;
            }
        }

        public List<spec> GetSpecialties()
        {
            using (SqlConnection connection = db.getConnection())
            {
                List<spec> specialties = new List<spec>();
                connection.Open();
                string query = "SELECT * FROM specialties"; //todo
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    spec spec = new spec();
                    spec.name = reader["specialty"].ToString();
                    spec.id = reader["id"].ToString();
                    specialties.Add(spec);
                }

                return specialties;
            }
        }

        public struct schedule
        {
            public List<string> week_days;
            public string id;
            public string start_time;

        }
        public void FillDates(string doc_id)
        {
            //todo: две недели
            using (SqlConnection connection = db.getConnection())
            {
                // TODO: get current date
                //List<schedule> schedule = new List<schedule>();
                schedule schedule = new schedule();
                //schedule.
                connection.Open();

                string getDaysOfWork = "SELECT DISTINCT week_day FROM schedule WHERE doctor_id = @doc_id";// -- узнаём, по каким дням недели работает док
                // qw data type
                // todo: use GetNextWeekday - как передавать день недели по номеру?
                SqlCommand getWeekDays = new SqlCommand(getDaysOfWork, connection);
                getWeekDays.Parameters.AddWithValue("@doc_id", doc_id); //qw

                SqlDataReader reader = getWeekDays.ExecuteReader();
                schedule.week_days = new List<string>();
                while (reader.Read()) //todo
                {
                    schedule.week_days.Add(reader["week_day"].ToString()); //получаем список дней недели, по к-м работает (int.ToString's)
                }

                // получаем даты рабочих дней врача на ближайшие две недели
                for (int i = 0; i < schedule.week_days.Count; i++)
                {
                    dateComboBox.Items.Add(GetNextWeekday(DateTime.Today.AddDays(1), ConvertIntsToDaysOfWeek(Convert.ToInt32(schedule.week_days[i])))); // next
                    //dateComboBox.Items.Add(GetNextWeekday(DateTime.Today.AddDays(8), ConvertIntsToDaysOfWeek(Convert.ToInt32(schedule.week_days[i])))); //after next
                }

                for (int i = 0; i < schedule.week_days.Count; i++)
                {
                    dateComboBox.Items.Add(GetNextWeekday(DateTime.Today.AddDays(8), ConvertIntsToDaysOfWeek(Convert.ToInt32(schedule.week_days[i])))); //after next
                }
            }
        }

        public List<appointments> GetAppointments(string doc_id)
        {
            throw new NotImplementedException();
        }

        private void PatientAppointmentForm_Load(object sender, EventArgs e)
        {
            specs = GetSpecialties();
            for (int i = 0; i < specs.Count; i++)
            {
                specComboBox.Items.Add(specs[i].name);
            }


            /*пока не выбраны предыдущие элементы, не отображать следующий. 
             * либо использовать hidden и показывать только после выбора предыдущего пункта
             либо отображать пустой дропбокс, пока не выбраны предыдущ. этапы. ! если есть возможность пустого выбора,
             то второй вариант предпочтительнее, т.к. при первом можно опять выбрать ничего. !? как убрать пустой выбор - ?*/
        }

        private void specComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            docComboBox.Items.Clear();
            doctors.Clear();
            string selectedSpec = specComboBox.Text;
            selectedSpecId = specs.Find(item => item.name == selectedSpec).id; //todo

            doctors = GetDoctors(selectedSpecId); //todo
            for (int i = 0; i < doctors.Count; i++)
            {
                docComboBox.Items.Add(doctors[i].fio);
            }
        }

        private void docComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            dateComboBox.Items.Clear();
            selectedDocId = doctors.Find(item => item.fio == docComboBox.Text).id;
            FillDates(selectedDocId);
        }

        private void timeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedScheduleId = schedules.Find(item => item.start_time == timeComboBox.Text).id; // qw test
        }

        public static int ConvertWDToInt(DayOfWeek day)
        {
            if (day == DayOfWeek.Monday)
                return 1;
            else if (day == DayOfWeek.Tuesday)
                return 2;
            else if (day == DayOfWeek.Wednesday)
                return 3;
            else if (day == DayOfWeek.Thursday)
                return 4;
            else if (day == DayOfWeek.Friday)
                return 5;
            else if (day == DayOfWeek.Saturday)
                return 6;
            else if (day == DayOfWeek.Sunday)
                return 7;
            else
                throw new Exception("Wrong day");
        }

        private void dateComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            timeComboBox.Items.Clear();
            selectedDate = Convert.ToDateTime(dateComboBox.Text);
            DayOfWeek selectedWD = selectedDate.DayOfWeek; //

            using (SqlConnection connection = db.getConnection())
            {
                
                connection.Open();
                string query = "SELECT id, start_time FROM schedule WHERE doctor_id = @doc_id AND week_day = @wd"; //todo
                SqlCommand command = new SqlCommand(query, connection);

                command.Parameters.AddWithValue("@doc_id", selectedDocId);
                command.Parameters.AddWithValue("@wd", ConvertWDToInt(selectedWD));


                // определяем, какие временные промежутки данного врача для данной даты уже заняты
                List<string> occupiedTime = new List<string>();
                string getOccupiedSchId = "SELECT schedule_id FROM appointments WHERE date = @date"; //todo
                SqlCommand selectOccupied = new SqlCommand(getOccupiedSchId, connection);
                selectOccupied.Parameters.AddWithValue("@date", selectedDate);
                SqlDataReader reader1 = selectOccupied.ExecuteReader();
                while (reader1.Read())
                {
                    occupiedTime.Add(reader1["schedule_id"].ToString());
                }
                reader1.Close();

                schedules = new List<schedule>();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    schedule schedule = new schedule();
                    
                    string scheduleId = reader["id"].ToString();
                    schedule.id = scheduleId;

                    string startTime = reader["start_time"].ToString();

                    schedule.start_time = startTime;
                    schedules.Add(schedule);

                    if (!occupiedTime.Contains(scheduleId)) // добавляем только свободное время для записи
                        timeComboBox.Items.Add(startTime);
                }
                reader.Close();
               
            }

        }
    }
}
