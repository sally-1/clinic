﻿namespace Clinic
{
    partial class PatientMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatientMainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.appointmentHistButton = new System.Windows.Forms.Button();
            this.makeAppointmentButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.fioLable = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.BackgroundImage = global::Clinic.Properties.Resources.bg3;
            this.panel1.Controls.Add(this.appointmentHistButton);
            this.panel1.Controls.Add(this.makeAppointmentButton);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(454, 380);
            this.panel1.TabIndex = 1;
            // 
            // appointmentHistButton
            // 
            this.appointmentHistButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.appointmentHistButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.appointmentHistButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.appointmentHistButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.appointmentHistButton.Location = new System.Drawing.Point(96, 244);
            this.appointmentHistButton.Name = "appointmentHistButton";
            this.appointmentHistButton.Size = new System.Drawing.Size(276, 44);
            this.appointmentHistButton.TabIndex = 8;
            this.appointmentHistButton.Text = "История приёмов";
            this.appointmentHistButton.UseVisualStyleBackColor = false;
            this.appointmentHistButton.Click += new System.EventHandler(this.appointmentHistButton_Click);
            // 
            // makeAppointmentButton
            // 
            this.makeAppointmentButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.makeAppointmentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.makeAppointmentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.makeAppointmentButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.makeAppointmentButton.Location = new System.Drawing.Point(96, 162);
            this.makeAppointmentButton.Name = "makeAppointmentButton";
            this.makeAppointmentButton.Size = new System.Drawing.Size(276, 44);
            this.makeAppointmentButton.TabIndex = 7;
            this.makeAppointmentButton.Text = "Записаться на приём";
            this.makeAppointmentButton.UseVisualStyleBackColor = false;
            this.makeAppointmentButton.Click += new System.EventHandler(this.makeAppointmentButton_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(186)))), ((int)(((byte)(219)))));
            this.panel2.Controls.Add(this.fioLable);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(454, 100);
            this.panel2.TabIndex = 0;
            // 
            // fioLable
            // 
            this.fioLable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fioLable.Font = new System.Drawing.Font("HCR Dotum Ext", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fioLable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(244)))), ((int)(((byte)(235)))));
            this.fioLable.Location = new System.Drawing.Point(0, 0);
            this.fioLable.Name = "fioLable";
            this.fioLable.Size = new System.Drawing.Size(454, 100);
            this.fioLable.TabIndex = 1;
            this.fioLable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PatientMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 380);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PatientMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поликлиника №6";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PatientMainForm_FormClosing);
            this.Load += new System.EventHandler(this.PatientMainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label fioLable;
        private System.Windows.Forms.Button appointmentHistButton;
        private System.Windows.Forms.Button makeAppointmentButton;
    }
}