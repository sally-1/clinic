﻿namespace Clinic
{
    partial class AdminMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminMainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.usersButton = new System.Windows.Forms.Button();
            this.specialtiesButton = new System.Windows.Forms.Button();
            this.scheduleButton = new System.Windows.Forms.Button();
            this.rolesButton = new System.Windows.Forms.Button();
            this.patientButton = new System.Windows.Forms.Button();
            this.doctorsButton = new System.Windows.Forms.Button();
            this.appointmentsButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.BackgroundImage = global::Clinic.Properties.Resources.bg3;
            this.panel1.Controls.Add(this.usersButton);
            this.panel1.Controls.Add(this.specialtiesButton);
            this.panel1.Controls.Add(this.scheduleButton);
            this.panel1.Controls.Add(this.rolesButton);
            this.panel1.Controls.Add(this.patientButton);
            this.panel1.Controls.Add(this.doctorsButton);
            this.panel1.Controls.Add(this.appointmentsButton);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 450);
            this.panel1.TabIndex = 2;
            // 
            // usersButton
            // 
            this.usersButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.usersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.usersButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usersButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.usersButton.Location = new System.Drawing.Point(309, 293);
            this.usersButton.Name = "usersButton";
            this.usersButton.Size = new System.Drawing.Size(176, 70);
            this.usersButton.TabIndex = 12;
            this.usersButton.Text = "Учётные записи";
            this.usersButton.UseVisualStyleBackColor = false;
            this.usersButton.Click += new System.EventHandler(this.usersButton_Click_1);
            // 
            // specialtiesButton
            // 
            this.specialtiesButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.specialtiesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.specialtiesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specialtiesButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.specialtiesButton.Location = new System.Drawing.Point(579, 204);
            this.specialtiesButton.Name = "specialtiesButton";
            this.specialtiesButton.Size = new System.Drawing.Size(176, 44);
            this.specialtiesButton.TabIndex = 11;
            this.specialtiesButton.Text = "Вр. профили";
            this.specialtiesButton.UseVisualStyleBackColor = false;
            this.specialtiesButton.Click += new System.EventHandler(this.specialtiesButton_Click);
            // 
            // scheduleButton
            // 
            this.scheduleButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.scheduleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.scheduleButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.scheduleButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.scheduleButton.Location = new System.Drawing.Point(309, 204);
            this.scheduleButton.Name = "scheduleButton";
            this.scheduleButton.Size = new System.Drawing.Size(176, 58);
            this.scheduleButton.TabIndex = 10;
            this.scheduleButton.Text = "Расписание врачей";
            this.scheduleButton.UseVisualStyleBackColor = false;
            this.scheduleButton.Click += new System.EventHandler(this.scheduleButton_Click);
            // 
            // rolesButton
            // 
            this.rolesButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.rolesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rolesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rolesButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.rolesButton.Location = new System.Drawing.Point(67, 204);
            this.rolesButton.Name = "rolesButton";
            this.rolesButton.Size = new System.Drawing.Size(176, 44);
            this.rolesButton.TabIndex = 9;
            this.rolesButton.Text = "Роли";
            this.rolesButton.UseVisualStyleBackColor = false;
            this.rolesButton.Click += new System.EventHandler(this.rolesButton_Click);
            // 
            // patientButton
            // 
            this.patientButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.patientButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.patientButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.patientButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.patientButton.Location = new System.Drawing.Point(579, 131);
            this.patientButton.Name = "patientButton";
            this.patientButton.Size = new System.Drawing.Size(176, 44);
            this.patientButton.TabIndex = 8;
            this.patientButton.Text = "Пациенты";
            this.patientButton.UseVisualStyleBackColor = false;
            this.patientButton.Click += new System.EventHandler(this.patientButton_Click);
            // 
            // doctorsButton
            // 
            this.doctorsButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.doctorsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doctorsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.doctorsButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.doctorsButton.Location = new System.Drawing.Point(309, 131);
            this.doctorsButton.Name = "doctorsButton";
            this.doctorsButton.Size = new System.Drawing.Size(176, 44);
            this.doctorsButton.TabIndex = 7;
            this.doctorsButton.Text = "Врачи";
            this.doctorsButton.UseVisualStyleBackColor = false;
            this.doctorsButton.Click += new System.EventHandler(this.doctorsButton_Click);
            // 
            // appointmentsButton
            // 
            this.appointmentsButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.appointmentsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.appointmentsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.appointmentsButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.appointmentsButton.Location = new System.Drawing.Point(67, 131);
            this.appointmentsButton.Name = "appointmentsButton";
            this.appointmentsButton.Size = new System.Drawing.Size(176, 44);
            this.appointmentsButton.TabIndex = 6;
            this.appointmentsButton.Text = "Приёмы";
            this.appointmentsButton.UseVisualStyleBackColor = false;
            this.appointmentsButton.Click += new System.EventHandler(this.appointmentsButton_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(186)))), ((int)(((byte)(219)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 100);
            this.panel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("HCR Dotum Ext", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(244)))), ((int)(((byte)(235)))));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(800, 100);
            this.label1.TabIndex = 1;
            this.label1.Text = "Администрирование";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AdminMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdminMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поликлиника №6";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminMainForm_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button usersButton;
        private System.Windows.Forms.Button specialtiesButton;
        private System.Windows.Forms.Button scheduleButton;
        private System.Windows.Forms.Button rolesButton;
        private System.Windows.Forms.Button patientButton;
        private System.Windows.Forms.Button doctorsButton;
        private System.Windows.Forms.Button appointmentsButton;
    }
}