﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinic
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        DB db = new DB();
        //SqlCommand command = new SqlCommand();
        //command.Connection = db.getConnetcion();

        public bool check_user(string login, string pass) // TODO
        {
            //DB db = new DB();   // qw

            DataTable table = new DataTable();

            SqlDataAdapter adapter = new SqlDataAdapter();

            SqlCommand command = new SqlCommand("SELECT * FROM users WHERE login=@log AND password=@pass", db.getConnection());

            command.Parameters.Add("@log", SqlDbType.NVarChar).Value = login;
            command.Parameters.Add("@pass", SqlDbType.NVarChar).Value = pass;

            using (db.getConnection())
            {
                adapter.SelectCommand = command;
                adapter.Fill(table);    // TODO: read docs about adapters and tables c#
                if (table.Rows.Count > 0)
                    return true;
                else
                    return false;
            }


            //SqlDataReader sqlReader = null;
            ////SqlCommand command = new SqlCommand("SELECT", sqlConnection);
            //cmd.CommandText = @"SELECT login, password 
            //    WHERE"; // todo
            //try
            //{
            //    sqlReader = cmd.ExecuteReader();
            //    while (sqlReader.Read()){
            //        dbLogin = sqlReader["login"].ToString();
            //        dbPass = sqlReader["password"].ToString();
            //    }
            //    // TODO достать пароль и логин из базы
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message.ToString(), ex.Source.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //finally
            //{
            //    if (sqlReader != null)
            //        sqlReader.Close();
            //}

            //if (login == dbLogin && pass == dbPass)
            //    return true;
            //else
            //    return false;
        }


        public string getUserRole(string login)
        {
            //TODO
            string role = "";
            string query = "SELECT role, users.id FROM roles JOIN users ON users.role_id = roles.id WHERE login = @login;";

            //DB db = new DB();

            using (SqlConnection connection = db.getConnection())
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@login", SqlDbType.NVarChar).Value = login;


                connection.Open();
                //db.openConnection();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    role = reader["role"].ToString(); //qw
                    Patient.user_id = reader["id"].ToString();
                }

                // Call Close when done reading.
                reader.Close();
            }
            return role;
        }


        public void getPatientData(string user_id)
        {
            // TODO
            string query = "SELECT * FROM patients JOIN users ON users.id=user_id WHERE user_id = @user_id;";

            //DB db = new DB();

            using (SqlConnection connection = db.getConnection())
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@user_id", SqlDbType.NVarChar).Value = user_id;


                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Patient.id = reader["id"].ToString();
                    Patient.name = reader["name"].ToString();
                    Patient.last_name = reader["last_name"].ToString();
                    Patient.patronymic = reader["patronymic"].ToString();
                    //Patient.oms
                }

                // Call Close when done reading.
                reader.Close();
            }
        }

        public string Hash(string input) //todo
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                byte[] hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                StringBuilder sb = new StringBuilder(hash.Length * 2);
                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }
                return sb.ToString();
            }
        }

        private void authButton_Click(object sender, EventArgs e)
        {
            string login = loginField.Text;
            string pass = passField.Text;

            if (check_user(login, Hash(pass))) 
            {
                if (getUserRole(login) == "admin")
                {
                    this.Hide();
                    AdminMainForm adminMainForm = new AdminMainForm();
                    adminMainForm.Show();
                }
                else if (getUserRole(login) == "patient")
                {
                    //Patient.id = ... ; //todo
                    //Patient patient = new Patient(name: "Ирина", "Салова", "Александровна");//todo
                    getPatientData(Patient.user_id);
                    this.Hide();
                    PatientMainForm patientMainForm = new PatientMainForm();
                    patientMainForm.Owner = this;
                    patientMainForm.Show();
                }
            }

            else
            {
                MessageBox.Show("Введён неверный логин или пароль");
            }
                
            //sqlConnection.Dispose();  // qw
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            loginField.Text = "salova@mail.ru"; //todo rm
            passField.Text = "password";

            //string connectionString = @"Data Source=DESKTOP-KM2U8DB\SQLEXPRESS;Initial Catalog=clinic;Integrated Security=True";
            //sqlConnection = new SqlConnection(connectionString);
            //try
            //{
            //    sqlConnection.Open();
            //    cmd.Connection = sqlConnection;
            //    //cmd.CommandText = "";

            //}
            //catch (SqlException ex)
            //{
            //    MessageBox.Show((ex.Message));
            //}
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit(); // qw
        }
    }
}
