﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Clinic
{
    public partial class UsersTableForm : Form
    {
        private SqlConnection sqlConnection = null;
        private SqlCommandBuilder sqlCommandBuilder = null;
        private SqlDataAdapter sqlDataAdapter = null;
        private DataSet dataSet = null;
        DB db = new DB();
        private bool addingNewRow = false;

        public UsersTableForm()
        {
            InitializeComponent();
        }

        private void getData()
        {
            try
            {
                var query = "SELECT *, 'Delete' AS [Operation] FROM users"; //
                sqlDataAdapter = new SqlDataAdapter(query, sqlConnection);

                sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);

                sqlCommandBuilder.GetInsertCommand();
                sqlCommandBuilder.GetUpdateCommand();
                sqlCommandBuilder.GetDeleteCommand();

                dataSet = new DataSet();

                sqlDataAdapter.Fill(dataSet, "users"); //

                dataGridView1.DataSource = dataSet.Tables["users"]; //

                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[4, i] = linkCell; //
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                
        }

        private void refreshData()
        {
            try
            {
                dataSet.Tables["users"].Clear();

                sqlDataAdapter.Fill(dataSet, "users"); //

                dataGridView1.DataSource = dataSet.Tables["users"]; //

                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[4, i] = linkCell; //
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void toolStripButton1_Click(object sender, EventArgs e) //qw а надо ли оно?
        {
            refreshData();

        }

        private void UsersTableForm_Load(object sender, EventArgs e)
        {
            //using (sqlConnection = db.getConnection())  //qw 
            //{

            //}

            sqlConnection = db.getConnection();
            sqlConnection.Open();
            getData();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4) //
                {
                    string command = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString(); //
                    if (command == "Insert")
                    {
                        int rowIndex = dataGridView1.Rows.Count - 2;
                        DataRow row = dataSet.Tables["users"].NewRow(); //

                        //row[""] = dataGridView1.Rows[rowIndex].Cells[""].Value;
                        // не считая АЙДИ!
                        row["login"] = dataGridView1.Rows[rowIndex].Cells["login"].Value;
                        row["password"] = dataGridView1.Rows[rowIndex].Cells["password"].Value;
                        row["role_id"] = dataGridView1.Rows[rowIndex].Cells["role_id"].Value;

                        dataSet.Tables["users"].Rows.Add(row); //

                        dataSet.Tables["users"].Rows.RemoveAt(dataSet.Tables["users"].Rows.Count - 1);

                        dataGridView1.Rows.RemoveAt(dataGridView1.Rows.Count - 2);

                        try
                        {

                            sqlDataAdapter.Update(dataSet, "users"); //
                            dataGridView1.Rows[e.RowIndex].Cells[4].Value = "Delete"; //

                            
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            addingNewRow = false;
                        }

                    }
                    else if (command == "Update")
                    {
                        int r = e.RowIndex;

                        //dataSet.Tables["users"].Rows[r][""] = dataGridView1.Rows[r].Cells[""].Value;
                        // не считая АЙДИ!

                        dataSet.Tables["users"].Rows[r]["login"] = dataGridView1.Rows[r].Cells["login"].Value;
                        dataSet.Tables["users"].Rows[r]["password"] = dataGridView1.Rows[r].Cells["password"].Value;
                        dataSet.Tables["users"].Rows[r]["role_id"] = dataGridView1.Rows[r].Cells["role_id"].Value;

                        try
                        {
                            sqlDataAdapter.Update(dataSet, "users"); //
                            dataGridView1.Rows[r].Cells[4].Value = "Delete"; //
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else if (command == "Delete")
                    {
                        if (MessageBox.Show("Вы уверены?", "Удаление строки", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                            == DialogResult.Yes) // пользователь подтвердил => удаляем
                        {
                            int rowIndex = e.RowIndex;
                            dataGridView1.Rows.RemoveAt(rowIndex);
                            dataSet.Tables["users"].Rows[rowIndex].Delete(); //

                            sqlDataAdapter.Update(dataSet, "users"); //
                        }
                    }
                    //addingNewRow = false; //qw
                    refreshData();
                }

               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {

        }

        private void dataGridView1_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                if (addingNewRow == false)
                {
                    addingNewRow = true;

                    int lastRow = dataGridView1.Rows.Count - 2;

                    DataGridViewRow row = dataGridView1.Rows[lastRow];

                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();

                    dataGridView1[4, lastRow] = linkCell; //

                    row.Cells["Operation"].Value = "Insert";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (addingNewRow == false)
                {
                    int rowIndex = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow editedRow = dataGridView1.Rows[rowIndex];

                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[4, rowIndex] = linkCell; //

                    editedRow.Cells["Operation"].Value = "Update";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
