﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Clinic
{
    public partial class Роли : Form
    {
        public Роли()
        {
            InitializeComponent();
        }

        private SqlConnection sqlConnection = null;
        private SqlCommandBuilder sqlCommandBuilder = null;
        private SqlDataAdapter sqlDataAdapter = null;
        private DataSet dataSet = null;
        DB db = new DB();
        private bool addingNewRow = false;


        private void getData()
        {
            try
            {
                var query = "SELECT *, 'Delete' AS [Operation] FROM roles"; //
                sqlDataAdapter = new SqlDataAdapter(query, sqlConnection);

                sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);

                sqlCommandBuilder.GetInsertCommand();
                sqlCommandBuilder.GetUpdateCommand();
                sqlCommandBuilder.GetDeleteCommand();

                dataSet = new DataSet();

                sqlDataAdapter.Fill(dataSet, "roles"); //

                dataGridView1.DataSource = dataSet.Tables["roles"]; //

                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[2, i] = linkCell; //
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void refreshData()
        {
            try
            {
                dataSet.Tables["roles"].Clear(); //

                sqlDataAdapter.Fill(dataSet, "roles"); //

                dataGridView1.DataSource = dataSet.Tables["roles"]; //

                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[2, i] = linkCell; //
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Роли_Load(object sender, EventArgs e)
        {
            sqlConnection = db.getConnection();
            sqlConnection.Open();
            getData();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 2) //
                {
                    string command = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString(); //
                    if (command == "Insert")
                    {
                        int rowIndex = dataGridView1.Rows.Count - 2;
                        DataRow row = dataSet.Tables["roles"].NewRow(); //

                        //row[""] = dataGridView1.Rows[rowIndex].Cells[""].Value;
                        // не считая АЙДИ!
                        row["role"] = dataGridView1.Rows[rowIndex].Cells["role"].Value;

                        dataSet.Tables["roles"].Rows.Add(row); //

                        dataSet.Tables["roles"].Rows.RemoveAt(dataSet.Tables["roles"].Rows.Count - 1); //

                        dataGridView1.Rows.RemoveAt(dataGridView1.Rows.Count - 2);

                        try
                        {

                            sqlDataAdapter.Update(dataSet, "roles"); //
                            dataGridView1.Rows[e.RowIndex].Cells[2].Value = "Delete"; //


                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            addingNewRow = false;
                        }

                    }
                    else if (command == "Update")
                    {
                        int r = e.RowIndex;

                        //dataSet.Tables["users"].Rows[r][""] = dataGridView1.Rows[r].Cells[""].Value;
                        // не считая АЙДИ!

                        dataSet.Tables["roles"].Rows[r]["role"] = dataGridView1.Rows[r].Cells["role"].Value;

                        try
                        {
                            sqlDataAdapter.Update(dataSet, "roles"); //
                            dataGridView1.Rows[r].Cells[2].Value = "Delete"; //
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else if (command == "Delete")
                    {
                        if (MessageBox.Show("Вы уверены?", "Удаление строки", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                            == DialogResult.Yes) // пользователь подтвердил => удаляем
                        {
                            int rowIndex = e.RowIndex;
                            dataGridView1.Rows.RemoveAt(rowIndex);
                            dataSet.Tables["roles"].Rows[rowIndex].Delete(); //

                            sqlDataAdapter.Update(dataSet, "roles"); //
                        }
                    }
                    refreshData();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                if (addingNewRow == false)
                {
                    addingNewRow = true;

                    int lastRow = dataGridView1.Rows.Count - 2;

                    DataGridViewRow row = dataGridView1.Rows[lastRow];

                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();

                    dataGridView1[2, lastRow] = linkCell; //

                    row.Cells["Operation"].Value = "Insert";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (addingNewRow == false)
                {
                    int rowIndex = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow editedRow = dataGridView1.Rows[rowIndex];

                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[2, rowIndex] = linkCell; //

                    editedRow.Cells["Operation"].Value = "Update";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    
    }
}
