﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinic
{
    class DB
    {
        const string connectionString = @"Data Source=DESKTOP-KM2U8DB\SQLEXPRESS;Initial Catalog=clinic;Integrated Security=True";
        public SqlConnection connection = new SqlConnection(connectionString);
        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();
        }
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
                connection.Close();
        }
        //public SqlConnection getConnection()
        //{
        //    return connection;
        //}
        public SqlConnection getConnection()
        {
            return new SqlConnection(connectionString);
        }
    }
}
