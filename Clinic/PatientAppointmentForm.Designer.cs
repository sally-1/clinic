﻿namespace Clinic
{
    partial class PatientAppointmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatientAppointmentForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.timeComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.docComboBox = new System.Windows.Forms.ComboBox();
            this.appointButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.specComboBox = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.BackgroundImage = global::Clinic.Properties.Resources.bg3;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.timeComboBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.dateComboBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.docComboBox);
            this.panel1.Controls.Add(this.appointButton);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.specComboBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(336, 435);
            this.panel1.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(221)))), ((int)(((byte)(247)))));
            this.label5.Font = new System.Drawing.Font("HCR Dotum Ext", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(85, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(165, 32);
            this.label5.TabIndex = 12;
            this.label5.Text = "Время";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timeComboBox
            // 
            this.timeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.timeComboBox.FormattingEnabled = true;
            this.timeComboBox.Location = new System.Drawing.Point(70, 309);
            this.timeComboBox.Name = "timeComboBox";
            this.timeComboBox.Size = new System.Drawing.Size(197, 21);
            this.timeComboBox.TabIndex = 11;
            this.timeComboBox.SelectedIndexChanged += new System.EventHandler(this.timeComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(221)))), ((int)(((byte)(247)))));
            this.label4.Font = new System.Drawing.Font("HCR Dotum Ext", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(85, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 32);
            this.label4.TabIndex = 10;
            this.label4.Text = "Дата";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateComboBox
            // 
            this.dateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dateComboBox.FormattingEnabled = true;
            this.dateComboBox.Location = new System.Drawing.Point(70, 228);
            this.dateComboBox.Name = "dateComboBox";
            this.dateComboBox.Size = new System.Drawing.Size(197, 21);
            this.dateComboBox.TabIndex = 9;
            this.dateComboBox.SelectedIndexChanged += new System.EventHandler(this.dateComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(221)))), ((int)(((byte)(247)))));
            this.label1.Font = new System.Drawing.Font("HCR Dotum Ext", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(85, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 32);
            this.label1.TabIndex = 8;
            this.label1.Text = "Врач";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // docComboBox
            // 
            this.docComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.docComboBox.FormattingEnabled = true;
            this.docComboBox.Location = new System.Drawing.Point(70, 149);
            this.docComboBox.Name = "docComboBox";
            this.docComboBox.Size = new System.Drawing.Size(197, 21);
            this.docComboBox.TabIndex = 7;
            this.docComboBox.SelectedIndexChanged += new System.EventHandler(this.docComboBox_SelectedIndexChanged);
            // 
            // appointButton
            // 
            this.appointButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.appointButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.appointButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.appointButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(161)))), ((int)(((byte)(163)))));
            this.appointButton.Location = new System.Drawing.Point(72, 376);
            this.appointButton.Name = "appointButton";
            this.appointButton.Size = new System.Drawing.Size(195, 47);
            this.appointButton.TabIndex = 6;
            this.appointButton.Text = "Записаться";
            this.appointButton.UseVisualStyleBackColor = false;
            this.appointButton.Click += new System.EventHandler(this.appointButton_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(221)))), ((int)(((byte)(247)))));
            this.label3.Font = new System.Drawing.Font("HCR Dotum Ext", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(85, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 32);
            this.label3.TabIndex = 3;
            this.label3.Text = "Профиль врача";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // specComboBox
            // 
            this.specComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.specComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.specComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.specComboBox.FormattingEnabled = true;
            this.specComboBox.Location = new System.Drawing.Point(70, 74);
            this.specComboBox.Name = "specComboBox";
            this.specComboBox.Size = new System.Drawing.Size(197, 21);
            this.specComboBox.TabIndex = 0;
            this.specComboBox.SelectedIndexChanged += new System.EventHandler(this.specComboBox_SelectedIndexChanged);
            // 
            // PatientAppointmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 435);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PatientAppointmentForm";
            this.Text = "Записаться на приём";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PatientAppointmentForm_FormClosing);
            this.Load += new System.EventHandler(this.PatientAppointmentForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox specComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox timeComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox dateComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox docComboBox;
        private System.Windows.Forms.Button appointButton;
    }
}