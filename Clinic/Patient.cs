﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinic
{
    public class Patient
    {
        public static string id;
        public static string user_id;
        //static int user_id;
        public static string name;
        public static string last_name;
        public static string patronymic;
        //string oms;


        public Patient(string id, string user_id, string name, string last_name, string patronymic=null) //qw
        {
            Patient.user_id = user_id;
            Patient.id = id;
            Patient.name = name;
            Patient.last_name = last_name;
            Patient.patronymic = patronymic;
        }
    }

}
