﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinic
{
    public partial class ScheduleTableForm : Form
    {
        public ScheduleTableForm()
        {
            InitializeComponent();
        }

        private SqlConnection sqlConnection = null;
        private SqlCommandBuilder sqlCommandBuilder = null;
        private SqlDataAdapter sqlDataAdapter = null;
        private DataSet dataSet = null;
        DB db = new DB();
        private bool addingNewRow = false;

        const int additionalColInd = 5; // = row count including id
        const string dbName = "schedule"; //
        string[] colNames = { "doctor_id", "week_day", "start_time", "end_time" }; //

        private void getData()
        {
            try
            {
                var query = "SELECT *, 'Delete' AS [Operation] FROM schedule"; //
                sqlDataAdapter = new SqlDataAdapter(query, sqlConnection);

                sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);

                sqlCommandBuilder.GetInsertCommand();
                sqlCommandBuilder.GetUpdateCommand();
                sqlCommandBuilder.GetDeleteCommand();

                dataSet = new DataSet();

                sqlDataAdapter.Fill(dataSet, dbName);

                dataGridView1.DataSource = dataSet.Tables[dbName];

                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[additionalColInd, i] = linkCell;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void refreshData()
        {
            try
            {
                dataSet.Tables[dbName].Clear();

                sqlDataAdapter.Fill(dataSet, dbName);

                dataGridView1.DataSource = dataSet.Tables[dbName];

                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[additionalColInd, i] = linkCell;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void ScheduleTableForm_Load(object sender, EventArgs e)
        {
            sqlConnection = db.getConnection();
            sqlConnection.Open();
            getData();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == additionalColInd)
                {
                    string command = dataGridView1.Rows[e.RowIndex].Cells[additionalColInd].Value.ToString();
                    if (command == "Insert")
                    {
                        int rowIndex = dataGridView1.Rows.Count - 2;
                        DataRow row = dataSet.Tables[dbName].NewRow();

                        for (int i = 0; i < colNames.Length; i++)
                        {
                            row[colNames[i]] = dataGridView1.Rows[rowIndex].Cells[colNames[i]].Value;
                        }

                        dataSet.Tables[dbName].Rows.Add(row);

                        dataSet.Tables[dbName].Rows.RemoveAt(dataSet.Tables[dbName].Rows.Count - 1);

                        dataGridView1.Rows.RemoveAt(dataGridView1.Rows.Count - 2);

                        try
                        {

                            sqlDataAdapter.Update(dataSet, dbName);
                            dataGridView1.Rows[e.RowIndex].Cells[additionalColInd].Value = "Delete";


                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            addingNewRow = false;
                        }

                    }
                    else if (command == "Update")
                    {
                        int r = e.RowIndex;

                        for (int i = 0; i < colNames.Length; i++)
                        {
                            dataSet.Tables[dbName].Rows[r][colNames[i]] = dataGridView1.Rows[r].Cells[colNames[i]].Value;
                        }
                        try
                        {
                            sqlDataAdapter.Update(dataSet, dbName);
                            dataGridView1.Rows[r].Cells[additionalColInd].Value = "Delete";
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else if (command == "Delete")
                    {
                        if (MessageBox.Show("Вы уверены?", "Удаление строки", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                            == DialogResult.Yes) // пользователь подтвердил => удаляем
                        {
                            int rowIndex = e.RowIndex;
                            dataGridView1.Rows.RemoveAt(rowIndex);
                            dataSet.Tables[dbName].Rows[rowIndex].Delete();

                            sqlDataAdapter.Update(dataSet, dbName);
                        }
                    }
                    refreshData();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                if (addingNewRow == false)
                {
                    addingNewRow = true;

                    int lastRow = dataGridView1.Rows.Count - 2;

                    DataGridViewRow row = dataGridView1.Rows[lastRow];

                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();

                    dataGridView1[additionalColInd, lastRow] = linkCell;

                    row.Cells["Operation"].Value = "Insert";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (addingNewRow == false)
                {
                    int rowIndex = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow editedRow = dataGridView1.Rows[rowIndex];

                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
                    dataGridView1[additionalColInd, rowIndex] = linkCell;

                    editedRow.Cells["Operation"].Value = "Update";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
